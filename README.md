# Mandelbrot

## Features
* two versions: fast or precise
* initial zoom and position via arguments
* move and zoom via keys

## Installation
Build the executable with
```
cmake -B build/ &&
make
```
