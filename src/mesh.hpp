#pragma once

class Mesh {
    
    private:
        unsigned int vao, vbo, ebo;
        unsigned int indices_count;
  
        
    public:
        Mesh();
        ~Mesh();
        
        
    public:
        unsigned int get_vao();
        unsigned int get_indices_count();
};
