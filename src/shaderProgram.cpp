#include "shaderProgram.hpp"
#include <sstream>
#include <iostream>
#include <fstream>

ShaderProgram::ShaderProgram() {
    this->ID = 0;
}

ShaderProgram::ShaderProgram(const std::string& name) {
    int success;
    char infoLog[512];

    unsigned int vertex = compile_shader(GL_VERTEX_SHADER, name + ".vsh");
    unsigned int fragment = compile_shader(GL_FRAGMENT_SHADER, name + ".fsh");

    ID = glCreateProgram();
    glAttachShader(ID, vertex);
    glAttachShader(ID, fragment);
    glLinkProgram(ID);

    glGetProgramiv(ID, GL_LINK_STATUS, &success);
    if(!success) {
        glGetProgramInfoLog(ID, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << name << "\n" << infoLog << std::endl;
    }

    glUseProgram(ID);
    glDeleteShader(vertex);
    glDeleteShader(fragment);
}

ShaderProgram::~ShaderProgram() {
    glDeleteProgram(ID);
}


/**
 * compiles Shader and returns Shader-ID
 */
unsigned int ShaderProgram::compile_shader(unsigned int type, const std::string& path) {
    std::string shader_code;
    std::ifstream shader_file;
    shader_file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    try {
        shader_file.open(path);
        std::stringstream shader_stream;
        shader_stream << shader_file.rdbuf();
        shader_file.close();
        shader_code = shader_stream.str();
    } catch(const std::fstream::failure&) {
        std::cerr << "ERROR::SHADER::READING_OF_FILE_FAILED" << std::endl;
    }

    //compile
    int success;
    char info_log[512];
    unsigned int shaderID = glCreateShader(type);
    const char * src_ptr = shader_code.c_str();
    glShaderSource(shaderID, 1, &src_ptr, NULL);
    glCompileShader(shaderID);
    glGetShaderiv(shaderID, GL_COMPILE_STATUS, &success);

    if(!success) {
        glGetShaderInfoLog(shaderID, 512, NULL, info_log);
        std::cout << "ERROR::SHADER::COMPILATION_FAILED\n" << info_log << std::endl;
    }
    return shaderID;
}

/**
 * glUseProgramm(ID)
 */
void ShaderProgram::use() const {
    glUseProgram(ID);
}

/**
 * sets bool value to Uniform with given Name
 */
void ShaderProgram::setBool(const std::string &name, bool value) {
    glUniform1i(glGetUniformLocation(ID, name.c_str()), (int) value);
}

/**
 * sets Integer value to Uniform with given Name
 */
void ShaderProgram::setInt(const std::string &name, int value) {
    glUniform1i(glGetUniformLocation(ID, name.c_str()), value);
}

/**
 * sets float value to Uniform with given Name
 */
void ShaderProgram::setFloat(const std::string &name, float value) {
    glUniform1f(glGetUniformLocation(ID, name.c_str()), value);
}

/**
 * sets double value to Uniform with given Name
 */
void ShaderProgram::set_double(const std::string& name, double value) {
    glUniform1d(glGetUniformLocation(ID, name.c_str()), value);
}
