#include <iostream>
#include <vector>

#include "glad.h"
#include <GLFW/glfw3.h>

#include "shaderProgram.hpp"
#include "mesh.hpp"

int WIDTH = 720;
int HEIGHT = 480;
bool RESIZED = true;
bool moved = true;
bool zoomed = true;
bool verbose = false;

double movement_speed = 1.0;
double x_offset = 0;
double y_offset = 0;
double zoom = 150;
double rMax = 32;
int maxIter = 570;

void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
    WIDTH = width;
    HEIGHT = height;
    RESIZED = true;
}

void key_callback(GLFWwindow* glfw_window, int key, int scancode, int action, int mods) {
    if(key == GLFW_KEY_Q && action == GLFW_PRESS) {
        movement_speed = std::max(0.00000000000001, movement_speed*0.5);
    } 
    if(key == GLFW_KEY_E && action == GLFW_PRESS) {
        movement_speed = std::max(movement_speed*2, movement_speed);
    }
    if(key == GLFW_KEY_V && action == GLFW_PRESS) {
        verbose = !verbose;
    }
}

int main(int argc, char **argv) {
    
    std::vector<std::string> arguments;
    for(int i=1; i<argc; i++) {
        arguments.push_back(std::string(argv[i]));
    }
    
    std::string shader_name = "mandelbrot";
    while(arguments.size() > 0) {
        std::string s = arguments[0];
        if(s == "--help") {
            std::cout << "Possible arguments are:" << std::endl
            << "--help (Duh.)" << std::endl
            << "-fast" << std::endl
            << "-x <double>" << std::endl
            << "-y <double>" << std::endl
            << "-rMax <double>" << std::endl
            << "-maxIter <int>" << std::endl
            << "-v" << std::endl;
            return 0;
        } else if(s == "-f") {
            shader_name = "mandelbrot-fast";
        } else if(s == "-x") {
            arguments.erase(arguments.begin());
            x_offset = std::stod(arguments[0]);
        } else if(s == "-y") {
            arguments.erase(arguments.begin());
            y_offset = std::stod(arguments[0]);
        } else if(s == "-zoom") {
            arguments.erase(arguments.begin());
            zoom = std::stod(arguments[0]);
        } else if(s == "-rMax") {
            arguments.erase(arguments.begin());
            rMax = std::stod(arguments[0]);
        } else if(s == "-maxIter") {
            arguments.erase(arguments.begin());
            maxIter = std::stoi(arguments[0]);
        } else if(s == "-v") {
            verbose = true;
        }
        arguments.erase(arguments.begin());
    }
    
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    
    GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "Mandelbrötchen", NULL, NULL);
    if(window == NULL) {
        std::cout << "Failed to create Window" << std::endl;
        glfwTerminate();
    }
    glfwMakeContextCurrent(window);
    
    if(!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
        std::cout << "Failed to initialize glad" << std::endl;
        glfwTerminate();
    }
    
    ShaderProgram shader("shaders/" + shader_name);
    Mesh mesh;
    
    glViewport(0, 0, WIDTH, HEIGHT);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetKeyCallback(window, key_callback);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    
    shader.use();
    shader.setInt("maxIter", maxIter);
    shader.set_double("rMax", rMax*rMax);
    while(!glfwWindowShouldClose(window)) {
        if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
            glfwSetWindowShouldClose(window, true);
        } 
        
        if(glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
            y_offset += movement_speed;
            moved = true;
        } 
        if(glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
            y_offset -= movement_speed;
            moved = true;
        } 
        if(glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
            x_offset -= movement_speed;
            moved = true;
        } 
        if(glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
            x_offset += movement_speed;
            moved = true;
        } 
        
        double zoom_fac = zoom/32;
        if(glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS) {
            zoom -= zoom > zoom_fac ? zoom_fac : 0;
            zoomed = true;
        }
        if(glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS) {
            zoom = std::max(zoom+std::max(1.0, zoom_fac), zoom);
            zoomed = true;
        }
        
        
        bool change = RESIZED | zoomed | moved;
        
        if(RESIZED) {
            shader.setInt("width", WIDTH);
            shader.setInt("height", HEIGHT);
            RESIZED = false;
        }
        if(zoomed) {
            shader.set_double("zoom", zoom);
            zoomed = false;
        }
        if(moved) {
            shader.set_double("x_offset", x_offset);
            shader.set_double("y_offset", y_offset);
            moved = false;
        }
        
        if(change) {
            if(verbose) {
                std::cout << "x: " << x_offset << ", y: " << y_offset << ", zoom: " << zoom << std::endl;
            }
            glClear(GL_COLOR_BUFFER_BIT);
            glBindVertexArray(mesh.get_vao());
            glDrawElements(GL_TRIANGLES, mesh.get_indices_count(), GL_UNSIGNED_INT, 0);
            glfwSwapBuffers(window);
        }
        glfwPollEvents();
    }
    
    glfwTerminate();
    
    return 0;
}
