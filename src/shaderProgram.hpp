#pragma once

#include "glad.h"
#include <string>

class ShaderProgram {

    public: 
        unsigned int ID;

        
    public: 
        ShaderProgram();
        ShaderProgram(const std::string& name);
        ~ShaderProgram();

        
    private: 
        unsigned int compile_shader(unsigned int type, const std::string& path);
        
    public: 
        void use() const;
        void setBool(const std::string &name, bool value);
        void setInt(const std::string &name, int value);
        void setFloat(const std::string &name, float value);
        void set_double(const std::string &name, double value);
};
