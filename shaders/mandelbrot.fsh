#version 400 core

uniform int width;
uniform int height;

uniform double rMax;
uniform double zoom;
uniform int maxIter;

uniform double x_offset;
uniform double y_offset;

out vec4 color;

vec3 hsv_to_rgb(double H, double S, double V) {
    float h = float(H/double(60));
    float s = float(S);
    float v = float(V);
    
    int hi = int(h);
    float f = h - hi;
    float p = 0;
    //float p = v*(1-s);
    float q = v*(1-s*f);
    float t = v*(1-s*(1-f));
    
    if(hi == 0 || hi == 6) {    // rot
        return vec3(v, t, p);
    } else if(hi == 1) {        // grün
        return vec3(q, v, p);
    } else if(hi == 2) {        // grün
        return vec3(p, v, t);
    } else if(hi == 3) {        // blau
        return vec3(p, q, v);
    } else if(hi == 4) {        // blau
        return vec3(t, p, v);
    } else if(hi == 5) {        // rot
        return vec3(v, p, q);
    }
    return vec3(0.0f);
}

void main() {
    
    double zero = double(0);
    
    double c_r = (double(gl_FragCoord.x) - double(width >> 1))/double(zoom) + x_offset/double(width);
    double c_i = (double(gl_FragCoord.y) - double(height >> 1))/double(zoom) + y_offset/double(height);
    
    double z_r = zero;
    double z_i = zero;
    
    double z_abs = zero;
    
    int iter = 0;
    while(iter < maxIter && z_abs < rMax) {
        double z_rn = z_r*z_r - z_i*z_i;
        double z_in = z_r*z_i*double(2);
        
        z_r = z_rn + c_r;
        z_i = z_in + c_i;
        
        z_abs = z_r * z_r + z_i * z_i;
        iter = iter + 1;
    }
    
    double h = double(iter*(maxIter-1)%maxIter);
    //double h = double(iter)/double(maxIter)*double(359);
    double s = double(1.0);
    double v = iter<maxIter ? double(1.0) : double(0.0);
        
    color = vec4(hsv_to_rgb(h, s, v), 1.0f);
}
