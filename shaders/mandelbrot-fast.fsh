#version 400 core

uniform int width;
uniform int height;

uniform double rMax;
uniform double zoom;
uniform int maxIter;

uniform double x_offset;
uniform double y_offset;

out vec4 color;

vec3 hsv_to_rgb(double H, double S, double V) {
    float h = float(H/double(60));
    float s = float(S);
    float v = float(V);
    
    int hi = int(h);
    float f = h - hi;
    float p = v*(1-s);
    float q = v*(1-s*f);
    float t = v*(1-s*(1-f));
    
    if(hi == 0 || hi == 6) {    // rot
        return vec3(v, t, p);
    } else if(hi == 1) {        // grün
        return vec3(q, v, p);
    } else if(hi == 2) {        // grün
        return vec3(p, v, t);
    } else if(hi == 3) {        // blau
        return vec3(p, q, v);
    } else if(hi == 4) {        // blau
        return vec3(t, p, v);
    } else if(hi == 5) {        // rot
        return vec3(v, p, q);
    }
    return vec3(0.0f);
}

void main() {
    
    float c_r = (gl_FragCoord.x - width/2.0)/float(zoom) + float(x_offset)/width;
    float c_i = (gl_FragCoord.y - height/2.0)/float(zoom) + float(y_offset)/height;
    
    float z_r = 0;
    float z_i = 0;
    
    float z_abs = 0;
    
    int iter = 0;
    while(iter < maxIter && z_abs < float(rMax)) {
        float z_rn = z_r*z_r - z_i*z_i;
        float z_in = z_r*z_i + z_r*z_i;
        
        z_r = z_rn + c_r;
        z_i = z_in + c_i;
        
        z_abs = z_r * z_r + z_i * z_i;
        iter = iter + 1;
    }
    
    double h = double(iter*(maxIter-1)%maxIter);
    double s = double(1.0);
    double v = iter<maxIter ? double(1.0) : double(0.0);
        
    color = vec4(hsv_to_rgb(h, s, v), 1.0f);
}
